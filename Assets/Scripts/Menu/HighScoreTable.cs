﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HighScoreTable : MonoBehaviour
{
    Transform entryContainer;
    Transform entryTemplate;
    Transform playerEntryTemplate;

    dreamloLeaderBoard dLeaderBoard;

    float templateHeight = 35f;

    int seconds = 0;

    void Start ()
    {
        entryContainer = transform.Find ("Table");
        entryTemplate = entryContainer.Find ("entryTemplate");
        playerEntryTemplate = entryContainer.Find ("inputTemplate");
        dLeaderBoard = dreamloLeaderBoard.GetSceneDreamloLeaderboard();

        entryTemplate.gameObject.SetActive (false);
        playerEntryTemplate.gameObject.SetActive (false);

        seconds = Random.Range (10, 200);
        //dLeaderBoard.GetScoresTimeAsc ();
        dreamloLeaderBoard.Score[] scoreList = dLeaderBoard.ToScoreArrayAroundTime(seconds, 9);

        //TODO: what if it is a first record?
        if (scoreList == null) 
        {
            //GUILayout.Label("(loading...)");
        } 
        else 
        {
            int pos = 0;
            bool drawPlayerEntry = true;
            foreach (dreamloLeaderBoard.Score currentScore in scoreList)
            {
                if (currentScore.seconds <= seconds)
                    DrawEntry (pos, currentScore.rank.ToString(),
                                    currentScore.playerName,
                                    currentScore.seconds.ToString());
                else
                {
                    if (drawPlayerEntry)
                    {
                        DrawPlayerEntry (pos, currentScore.rank.ToString(), seconds.ToString() );
                        drawPlayerEntry = false;
                    }
                    DrawEntry (pos+1, (currentScore.rank + 1).ToString(),
                                    currentScore.playerName,
                                    currentScore.seconds.ToString());
                }
                pos++;
            }
        }
    }

    void DrawEntry (int pos, string rankText, string nameText, string timeText)
    {
        Transform entryTransform = Instantiate (entryTemplate, entryContainer);
        RectTransform entryRectTransform = entryTransform.GetComponent<RectTransform> ();
        entryRectTransform.anchoredPosition = new Vector2 (0, -templateHeight * pos);
        entryTransform.gameObject.SetActive (true);
        entryTransform.GetComponent<Image> ().enabled = (pos % 2 == 1);

        entryTransform.Find ("RankText").GetComponent<TextMeshProUGUI> ().SetText (rankText);
        entryTransform.Find ("NameText").GetComponent<TextMeshProUGUI> ().SetText (nameText);
        entryTransform.Find ("TimeText").GetComponent<TextMeshProUGUI> ().SetText (timeText);        
    }

    void DrawPlayerEntry (int pos, string rankText, string timeText)
    {
        Transform entryTransform = Instantiate (playerEntryTemplate, entryContainer);
        RectTransform entryRectTransform = entryTransform.GetComponent<RectTransform> ();
        entryRectTransform.anchoredPosition = new Vector2 (0, -templateHeight * pos);
        entryTransform.gameObject.SetActive (true);
        entryTransform.GetComponent<Image> ().enabled = (pos % 2 == 1);

        entryTransform.Find ("RankText").GetComponent<TextMeshProUGUI> ().SetText (rankText);
        entryTransform.Find ("TimeText").GetComponent<TextMeshProUGUI> ().SetText (timeText);        
    }
}
