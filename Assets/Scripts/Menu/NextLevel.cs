﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NextLevel : MonoBehaviour
{
    public ELevelNames levelName = ELevelNames.none;

    public void loadLevel()
    {
        LevelSettingsStatic.SetLevel (levelName);
        SceneManager.LoadScene("main");
    }

    public void endPromo()
    {
        SceneManager.LoadScene("endpromo");
    }

    public void menu()
    {
        SceneManager.LoadScene("levels_menu");
    }

    public void controls()
    {
        SceneManager.LoadScene("controls");
    }
}
