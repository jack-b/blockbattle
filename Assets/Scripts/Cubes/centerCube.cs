﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CenterCube : MonoBehaviour
{
    // TODO: better way to make this links static
    public Material redMaterial;
    public Material whiteMaterial;
    public Material yellowMaterial;
    public Material blueMaterial;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    
    public void changeColor(CubeColors col)
    {
        switch (col)
        {
        case CubeColors.red:
            this.GetComponent<Renderer>().material = redMaterial;
            break;
        case CubeColors.white:
            this.GetComponent<Renderer>().material = whiteMaterial;
            break;
        case CubeColors.yellow:
            this.GetComponent<Renderer>().material = yellowMaterial;
            break;
        case CubeColors.blue:
            this.GetComponent<Renderer>().material = blueMaterial;
            break;
        }
    }
}
