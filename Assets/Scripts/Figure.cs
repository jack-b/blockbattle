// Bug in RayCastToBase If ht null it is added to queue anyway with infinity value
using UnityEngine;
using System.Collections;
using System.Collections.Generic;



/* 
 * This class implement falling figure
 * 
 * Methods:
 * CreateFigure () - create new falling figure of random type
 * MoveFigure () - moves figure right/left/up/down if arrows keys have been pressed
 * bool RayCastToBase () - Casts rays from each cube element of figure to find nearest cube of base figure
 * AttachFigure () - recursively call itself to attach figure cubes concurrently (see full description in the method)
 * FirstChainBlock () - Initialize first chain
 * 
 */
public class Figure : MonoBehaviour 
{
    public GameLogic gameLogic; // Connection to other game objects in the game
    public Transform transparentCube; // transparent prefab cube for figure
    public Transform cubeEdges; // cube edges prefab cube for figure
    public Transform axis; // Axis game object
    public Transform cubeSelection; // Quad selection game object
    public ColorScheme colorScheme; // link for color scheme
    Dictionary<CubeColors,Color> colors;
    Vector3 down;

    public SoundEffects soundEffects;

    public bool isBottomView = false;
    public BottomFigure bottomFigure;

    public enum EHighlightType {none, cube, axis, square};

    public EHighlightType highlightType;

    public enum EFigureViewType {edge = 0, transparent = 1};    
    public EFigureViewType figureViewType = EFigureViewType.edge;
    Transform figureElement; // one link to transform based on figureViewType

    public enum EFigureType {L = 0, T = 1, S = 2, I = 3, O = 4, lt = 5, ll = 6, i3 = 7, i2 = 8, sl = 9}; 

    EFigureType figType; // type of figure

    public enum ERegime {create = 0, falling = 1, fell = 2}; 

    ERegime figRegime; // main state machine for figure

    public enum EMoveFigure {no = 0, up = 1, right = 2, down = 3, left = 4}; 

    protected EMoveFigure movement; // type of movement if arrow keys have been pressed

    float fallTimer; // Timer when to move figure towards the base figure
    public float fallSpeed = 1; // Seconds till figure fall at one level towards the base figure 

    public bool isPauseFalling = false;

    public int bornLevel = 8; // the level under initial basefigure's ground where figures will be born
    float bornZ; // calculated height

    protected ERotating rotating;
    public float rotationSpeed = 5.0f;
    protected float angle;
    Quaternion originalRotationValue;
    
    public float moveSpeed = 5.0f; // Pause when key pressed for movement won't count
//    float moveTimer = 0; // Timer for horizontal movement
    protected float move;

    public EDirection relativeDirection; // This direction shows from which direction the figure is falling onto baseFigure. It changes in CameraControl when rotating around baseFigure
//  EDirection direction;

    bool canFall; // true if figure can fall at one cube towards the base figure
    bool repeatDestroyColorChains; // true if there were hanging pieces and 
    // function DestroyColorChainsOfCubes () need to be called again (unity feature of raycast and moving colliders)

    List<CubePointPosition> figurePoints;

    RaycastHit hit; // ref to hit nearest cube of the base figure (GameObject)
    Color hitColor; 
    RaycastHit[] hits; // array of hit cubes of the base figure (GameObjects)
    int childHitIndex; // index of cube in a figure which hit nearest cube of the base figure
    public int levelTillFall; // how many levels left till figure fall

    List<KeyValuePair<int,float>> dist2base; // map of indexes of figure's cubes and the base figure's cubes their hit

    List<Transform> cubeSelections; // List of quat objects for cube highlighting
    public bool redrawSelection; // Evey time it's true cube highlithing will be redrawn

    public Dictionary<EFigureControlInterface,KeyCode> controlBinding;

    // For testing
    public bool _test_sameColor = true; 

    public int BornLevel
    {
        get { return bornLevel; }
        set {
            bornLevel = value;

            bornZ = - ( (float)bornLevel + (float)(gameLogic.baseFigure.pit.z - 1) / 2) * CubePointPosition.size + CubePointPosition.size / 2;
        }
    } 
    
    // Use this for initialization
    void Start () 
    {
        Transform temp;

        figRegime = ERegime.create;

        figurePoints = new List<CubePointPosition>();
        figurePoints.Add (new CubePointPosition(0));
        figurePoints.Add (new CubePointPosition(1));
        figurePoints.Add (new CubePointPosition(2));
        figurePoints.Add (new CubePointPosition(3));
        
        switch (figureViewType) 
        {
            case EFigureViewType.edge:
                figureElement = cubeEdges;
                break;
            case EFigureViewType.transparent:
                figureElement = transparentCube;
                break;
        }

        temp = Instantiate(figureElement, Vector3.zero, Quaternion.identity ) as Transform;
        // temp.gameObject.layer = 11;
        temp.parent = this.gameObject.transform;

        temp = Instantiate(figureElement, Vector3.zero, Quaternion.identity ) as Transform;
        // temp.gameObject.layer = 11;
        temp.parent = this.gameObject.transform;

        temp = Instantiate(figureElement, Vector3.zero, Quaternion.identity ) as Transform;
        // temp.gameObject.layer = 11;
        temp.parent = this.gameObject.transform;

        temp = Instantiate(figureElement, Vector3.zero, Quaternion.identity ) as Transform;
        // temp.gameObject.layer = 11;
        temp.parent = this.gameObject.transform;

        if (isBottomView)
            if ( bottomFigure != null )
            {
                foreach (Transform child in transform)
                    foreach (Transform child2 in child)
                        child2.GetComponent<LineRenderer> ().enabled = false;
            }
            else
            {
                isBottomView = false;
            }

        // TODO: Move all colors to Color Scheme
        colors = new Dictionary<CubeColors,Color> ();
        colors.Add ( CubeColors.red, new Color (188.0f/255.0f, 84.0f/255.0f, 84.0f/255.0f, 55.0f/255.0f) );
        colors.Add ( CubeColors.white, new Color (174.0f/255.0f, 174.0f/255.0f, 174.0f/255.0f, 55.0f/255.0f) );
        colors.Add ( CubeColors.yellow, new Color (255.0f/255.0f, 255.0f/255.0f, 73.0f/255.0f, 55.0f/255.0f) );
        colors.Add ( CubeColors.blue, new Color (23.0f/255.0f, 140.0f/255.0f, 255.0f/255.0f, 55.0f/255.0f) );

        originalRotationValue = transform.rotation;
        rotating = ERotating.no;
        movement = EMoveFigure.no;
        //Debug.Log ("Original rotation " + originalRotationValue.ToString());

        relativeDirection = EDirection.forward;

        dist2base = new List<KeyValuePair<int,float>>();
        dist2base.Add (new KeyValuePair<int,float>(0, 0.0f));
        dist2base.Add (new KeyValuePair<int,float>(1, 0.0f));
        dist2base.Add (new KeyValuePair<int,float>(2, 0.0f));
        dist2base.Add (new KeyValuePair<int,float>(3, 0.0f));

        hits = new RaycastHit[4];
        hitColor = Color.black;

        // Instantiate cube selection game objects
        // TODO: Maybe it's better to seperate selection from figure in its own class
        cubeSelections = new List<Transform>();
        for (int i = 0; i < 4; i++)
        {
            temp = Instantiate(cubeSelection, Vector3.zero, Quaternion.identity ) as Transform;
            temp.gameObject.SetActive (false);
            cubeSelections.Add (temp);
        }

        bornZ = - ( (float)bornLevel + (float)(gameLogic.baseFigure.pit.z - 1) / 2) * CubePointPosition.size + CubePointPosition.size / 2;

        controlBinding = StaticLogic.controlBinding;
    }
    
    // Update is called once per frame
    void Update () 
    {
        bool rotationFinished;

        EDirection dir = EDirection.right;
        if (StaticLogic.gameState == StaticLogic.EGameState.ingame)
        {
            switch (figRegime)
            {
            case ERegime.create:

                if (Camera.main.GetComponent<CameraControl>().rotating == ERotating.no)
                {
                    CreateFigure ();

                    if ( StaticLogic.figureDublicate != null )
                        StaticLogic.figureDublicate.CreateDublicate (transform);

                    if (isBottomView)
                        bottomFigure.Create ( transform );

                    figRegime = ERegime.falling;
                    
                    fallTimer = 0.0f;

                    repeatDestroyColorChains = false;

                    redrawSelection = true;
                }

                break;
            case ERegime.falling:

                rotationFinished = StaticLogic.RotateObject (ref rotating, ref angle, rotationSpeed, transform.position, transform);

                if (rotationFinished)
                   redrawSelection = true;
 
                MoveFigure ();

                if ( Input.GetKeyDown ( controlBinding [ EFigureControlInterface.next_color ] ) && (movement == EMoveFigure.no) && (Camera.main.GetComponent<CameraControl>().rotating == ERotating.no) )
                {
                    SwitchColor ();
                }

                if (movement == EMoveFigure.no)
                {
                    move = 0;
                    if ( Input.GetKeyDown ( controlBinding [ EFigureControlInterface.move_right ] ) )
                    {
                        movement = EMoveFigure.right;
                    }
                    else 
                    if ( Input.GetKeyDown ( controlBinding [ EFigureControlInterface.move_up ] ) )
                    {
                        movement = EMoveFigure.up;
                    }
                    else 
                    if ( Input.GetKeyDown ( controlBinding [ EFigureControlInterface.move_down ] ) )
                    {
                        movement = EMoveFigure.down;
                    }
                    else 
                    if ( Input.GetKeyDown ( controlBinding [ EFigureControlInterface.move_left ] ) )
                    {
                        movement = EMoveFigure.left;
                    }
                }

                if (rotating == ERotating.no)
                {
                    angle = 0;
                    if ( Input.GetKeyDown ( controlBinding [ EFigureControlInterface.turn_down ] ) )
                        rotating = ERotating.down;
                    else 
                    if ( Input.GetKeyDown ( controlBinding [ EFigureControlInterface.turn_up ] ) )
                        rotating = ERotating.up;
                    else 
                    if ( Input.GetKeyDown ( controlBinding [ EFigureControlInterface.turn_right ] ) )
                        rotating = ERotating.right;
                    else 
                    if ( Input.GetKeyDown ( controlBinding [ EFigureControlInterface.turn_left ] ) )
                        rotating = ERotating.left;
                    else 
                    if ( Input.GetKeyDown ( controlBinding [ EFigureControlInterface.turn_counterclockwise ] ) )
                        rotating = ERotating.counterclockwise;
                    else 
                    if ( Input.GetKeyDown ( controlBinding [ EFigureControlInterface.turn_clockwise ] ) )
                        rotating = ERotating.clockwise;

                    if ( StaticLogic.figureDublicate != null )
                    {
                        StaticLogic.figureDublicate.RotateDublicate ( rotating, rotationSpeed );
                    }
                }

                if ( Input.GetKeyDown ( controlBinding [ EFigureControlInterface.fall ] ) && (movement == EMoveFigure.no) && (rotating == ERotating.no) && (Camera.main.GetComponent<CameraControl>().rotating == ERotating.no) )
                {
                    figRegime = ERegime.fell;
                }

                canFall = RayCastToBase ();
                SetLevelTillFall ();

                if (!isPauseFalling)
                    fallTimer += Time.deltaTime;
                if (fallTimer >= fallSpeed)
                {
                    if (canFall)
                    {
                        transform.Translate (Camera.main.transform.forward * CubePointPosition.size, Space.World);
                    }
                    else
                    {
                        figRegime = ERegime.fell;
                        break;
                    }
                    fallTimer = 0.0f;
                }

               if (redrawSelection)
                {
                    Debug.Log ("Redraw selection");
                    DrawSelection ();

                    if (isBottomView)
                    {
                        if ( childHitIndex >= 0 )
                        {
                            //Debug.Log ( "Bottom pos: " + (hit.transform.position - CubePointPosition.size * Camera.main.transform.forward) + " ChildIndex: " + childHitIndex);
                            // TODO: works only for easy regime
                            bottomFigure.Draw ( hit.transform.position - CubePointPosition.size * Camera.main.transform.forward, childHitIndex );
                        }
                        else
                            bottomFigure.HideFigure ();
                    }
                }

                break;
            case ERegime.fell:
                bool isNeighbourGold = false;

                HideSelection ();
                HideFigure ();
                bottomFigure.HideFigure ();

                if (!repeatDestroyColorChains)
                {
                    if ( childHitIndex >= 0 )
                    {
                        if (figurePoints [childHitIndex].xRight_ == null)
                            dir = EDirection.left;
                        else
                            if (figurePoints [childHitIndex].xLeft_ == null)
                                dir = EDirection.right;
                        else
                            if (figurePoints [childHitIndex].yRight_ == null)
                                dir = EDirection.down;
                        else
                            if (figurePoints [childHitIndex].yLeft_ == null)
                                dir = EDirection.up;
                        else
                            if (figurePoints [childHitIndex].zRight_ == null)
                                dir = EDirection.backward;
                        else
                            if (figurePoints [childHitIndex].zLeft_ == null)
                                dir = EDirection.forward;

                        isNeighbourGold = AttachFigure (childHitIndex, hit.transform, relativeDirection, dir);

                        if (isNeighbourGold)
                            soundEffects.PlayingAttachingGoldCube ();
                        else
                            soundEffects.PlayingAttachingRegularCube ();
                    }
                }

                repeatDestroyColorChains = gameLogic.baseFigure.DestroyColorChainsOfCubes ();
                if (!repeatDestroyColorChains)
                    figRegime = ERegime.create;

                break;
            }
        }
    }

    public void Reset ()
    {
        figRegime = ERegime.create;
    }

    void CreateFigure ()
    {
        CubeColors c;
        int i;

        if (LevelSettingsStatic.gameDifficulty == EGameDifficulty.hard)
        {
            figType = (EFigureType) Random.Range (0, 7);
            transform.GetChild (0).gameObject.SetActive (true);
            transform.GetChild (1).gameObject.SetActive (true);                 
            transform.GetChild (2).gameObject.SetActive (true);
            transform.GetChild (3).gameObject.SetActive (true);                 
        } else
        if (LevelSettingsStatic.gameDifficulty == EGameDifficulty.easy)
        {
            figType = EFigureType.i2;
            transform.GetChild (0).gameObject.SetActive (true);
            transform.GetChild (1).gameObject.SetActive (true);                 
            transform.GetChild (2).gameObject.SetActive (false);
            transform.GetChild (3).gameObject.SetActive (false);                 
        } 
        else
        {
            if (Random.Range (0, 2) == 0)
                figType = EFigureType.i3;
            else
                figType = EFigureType.sl;
            transform.GetChild (0).gameObject.SetActive (true);
            transform.GetChild (1).gameObject.SetActive (true);                 
            transform.GetChild (2).gameObject.SetActive (true);
            transform.GetChild (3).gameObject.SetActive (false);                 
        }                    

        transform.eulerAngles = new Vector3(0, 0, 0);
        //Debug.Log ("Born rotation " + Camera.main.transform.rotation.ToString());

        transform.position = -gameLogic.baseFigure.dxInit * Camera.main.transform.right + gameLogic.baseFigure.dyInit * Camera.main.transform.up + bornZ * Camera.main.transform.forward;
        Debug.Log("Born position " + transform.position.ToString ());

        transform.GetChild (0).transform.position = transform.GetChild (1).transform.position 
            = transform.GetChild (2).transform.position = transform.GetChild (3).transform.position = transform.position;

        switch (figureViewType) 
        {
            case EFigureViewType.edge:
            
                i = Random.Range (0, 3);
                c = colorScheme.colors [i];
                transform.GetChild (0).GetComponent<CubeEdges>().setColor(c);
                if (!_test_sameColor)
                {
                    // no same color cubes in Easy regime
                    if (LevelSettingsStatic.gameDifficulty == EGameDifficulty.easy)
                        c = colorScheme.colors [ ( ( i + Random.Range (1, 3) ) % 3 ) ];
                    else
                        c = colorScheme.colors [ Random.Range (0, 3) ];
                }
                transform.GetChild (1).GetComponent<CubeEdges>().setColor(c);
                if (!_test_sameColor)
                   c = colorScheme.colors [ Random.Range (0, 3) ];
                transform.GetChild (2).GetComponent<CubeEdges>().setColor(c);
                if (!_test_sameColor)
                   c = colorScheme.colors [ Random.Range (0, 3) ];
                transform.GetChild (3).GetComponent<CubeEdges>().setColor(c);
                break;
                
            case EFigureViewType.transparent:
            
                c = colorScheme.colors [ Random.Range (0, 3) ];
                transform.GetChild (0).GetComponent<CubeAttributes>().color = c;
                transform.GetChild (0).GetComponent<Renderer>().material.SetColor("_Color", colors[c]);
                if (!_test_sameColor)
                   c = colorScheme.colors [ Random.Range (0, 3) ];
                transform.GetChild (1).GetComponent<CubeAttributes>().color = c;
                transform.GetChild (1).GetComponent<Renderer>().material.SetColor("_Color", colors[c]);
                if (!_test_sameColor)
                   c = colorScheme.colors [ Random.Range (0, 3) ];
                transform.GetChild (2).GetComponent<CubeAttributes>().color = c;
                transform.GetChild (2).GetComponent<Renderer>().material.SetColor("_Color", colors[c]);
                if (!_test_sameColor)
                   c = colorScheme.colors [ Random.Range (0, 3) ];
                transform.GetChild (3).GetComponent<CubeAttributes>().color = c;
                transform.GetChild (3).GetComponent<Renderer>().material.SetColor("_Color", colors[c]);
                break;
        }
        
        if (LevelSettingsStatic.figureColorsAtStart)
        {
            SetColors (LevelSettingsStatic.colorsAtStart[0], LevelSettingsStatic.colorsAtStart[1]);
            LevelSettingsStatic.figureColorsAtStart = false;
        }

        switch (figType)
        {
            case EFigureType.L:
                transform.GetChild (0).transform.Translate (- Camera.main.transform.right * CubePointPosition.size);

                transform.GetChild (2).transform.Translate (Camera.main.transform.right * CubePointPosition.size);      
                
                transform.GetChild (3).transform.Translate (Camera.main.transform.right * CubePointPosition.size 
                    + Camera.main.transform.up * CubePointPosition.size);

                figurePoints [0].SetOneNeighbourAND (EDirection.right, figurePoints [1]);
                figurePoints [1].SetOneNeighbourAND (EDirection.left, figurePoints [0]);
                figurePoints [1].SetOneNeighbourOR (EDirection.right, figurePoints [2]);
                figurePoints [2].SetOneNeighbourAND (EDirection.left, figurePoints [1]);
                figurePoints [2].SetOneNeighbourOR (EDirection.up, figurePoints [3]);
                figurePoints [3].SetOneNeighbourAND (EDirection.down, figurePoints [2]);

                break;
            case EFigureType.T:
                transform.GetChild (0).transform.Translate (- Camera.main.transform.right * CubePointPosition.size);

                transform.GetChild (2).transform.Translate (Camera.main.transform.right * CubePointPosition.size);

                transform.GetChild (3).transform.Translate (Camera.main.transform.up * CubePointPosition.size);

                figurePoints [0].SetOneNeighbourAND (EDirection.right, figurePoints [1]);
                figurePoints [1].SetOneNeighbourAND (EDirection.left, figurePoints [0]);
                figurePoints [1].SetOneNeighbourOR (EDirection.right, figurePoints [2]);
                figurePoints [1].SetOneNeighbourOR (EDirection.up, figurePoints [3]);
                figurePoints [2].SetOneNeighbourAND (EDirection.left, figurePoints [1]);
                figurePoints [3].SetOneNeighbourAND (EDirection.down, figurePoints [1]);

                break;
            case EFigureType.S:
                transform.GetChild (0).transform.Translate (- Camera.main.transform.right * CubePointPosition.size);

                transform.GetChild (2).transform.Translate (Camera.main.transform.up * CubePointPosition.size);

                transform.GetChild (3).transform.Translate (Camera.main.transform.right * CubePointPosition.size 
                    + Camera.main.transform.up * CubePointPosition.size);

                figurePoints [0].SetOneNeighbourAND (EDirection.right, figurePoints [1]);
                figurePoints [1].SetOneNeighbourAND (EDirection.left, figurePoints [0]);
                figurePoints [1].SetOneNeighbourOR (EDirection.up, figurePoints [2]);
                figurePoints [2].SetOneNeighbourAND (EDirection.down, figurePoints [1]);
                figurePoints [2].SetOneNeighbourOR (EDirection.right, figurePoints [3]);
                figurePoints [3].SetOneNeighbourAND (EDirection.left, figurePoints [2]);

                break;
            case EFigureType.I:
                transform.GetChild (0).transform.Translate (- Camera.main.transform.right * CubePointPosition.size);

                transform.GetChild (2).transform.Translate (Camera.main.transform.right * CubePointPosition.size);

                transform.GetChild (3).transform.Translate (Camera.main.transform.right * CubePointPosition.size * 2.0f);

                figurePoints [0].SetOneNeighbourAND (EDirection.right, figurePoints [1]);
                figurePoints [1].SetOneNeighbourAND (EDirection.left, figurePoints [0]);
                figurePoints [1].SetOneNeighbourOR (EDirection.right, figurePoints [2]);
                figurePoints [2].SetOneNeighbourAND (EDirection.left, figurePoints [1]);
                figurePoints [2].SetOneNeighbourOR (EDirection.right, figurePoints [3]);
                figurePoints [3].SetOneNeighbourAND (EDirection.left, figurePoints [2]);

                break;
            case EFigureType.O:
                transform.GetChild (0).transform.Translate (Camera.main.transform.right * CubePointPosition.size);

                transform.GetChild (2).transform.Translate (Camera.main.transform.up * CubePointPosition.size);

                transform.GetChild (3).transform.Translate (Camera.main.transform.right * CubePointPosition.size 
                    + Camera.main.transform.up * CubePointPosition.size);

                figurePoints [0].SetOneNeighbourAND (EDirection.left, figurePoints [1]);
                figurePoints [1].SetOneNeighbourAND (EDirection.right, figurePoints [0]);
                figurePoints [1].SetOneNeighbourOR (EDirection.up, figurePoints [2]);
                figurePoints [2].SetOneNeighbourAND (EDirection.down, figurePoints [1]);
                figurePoints [2].SetOneNeighbourOR (EDirection.right, figurePoints [3]);
                figurePoints [3].SetOneNeighbourAND (EDirection.left, figurePoints [2]);

                break;
            case EFigureType.lt:
                transform.GetChild (0).transform.Translate (Camera.main.transform.right * CubePointPosition.size);

                transform.GetChild (2).transform.Translate (Camera.main.transform.up * CubePointPosition.size);

                transform.GetChild (3).transform.Translate (Camera.main.transform.forward * CubePointPosition.size);

                figurePoints [0].SetOneNeighbourAND (EDirection.left, figurePoints [1]);
                figurePoints [1].SetOneNeighbourAND (EDirection.right, figurePoints [0]);
                figurePoints [1].SetOneNeighbourOR (EDirection.up, figurePoints [2]);
                figurePoints [1].SetOneNeighbourOR (EDirection.forward, figurePoints [3]);
                figurePoints [2].SetOneNeighbourAND (EDirection.down, figurePoints [1]);
                figurePoints [3].SetOneNeighbourAND (EDirection.backward, figurePoints [1]);

                break;
            case EFigureType.ll:
                transform.GetChild (0).transform.Translate (Camera.main.transform.right * CubePointPosition.size);

                transform.GetChild (2).transform.Translate (Camera.main.transform.up * CubePointPosition.size);

                transform.GetChild (3).transform.Translate (Camera.main.transform.forward * CubePointPosition.size 
                    + Camera.main.transform.up * CubePointPosition.size);

                figurePoints [0].SetOneNeighbourAND (EDirection.left, figurePoints [1]);
                figurePoints [1].SetOneNeighbourAND (EDirection.right, figurePoints [0]);
                figurePoints [1].SetOneNeighbourOR (EDirection.up, figurePoints [2]);
                figurePoints [2].SetOneNeighbourAND (EDirection.down, figurePoints [1]);
                figurePoints [2].SetOneNeighbourOR (EDirection.forward, figurePoints [3]);
                figurePoints [3].SetOneNeighbourAND (EDirection.backward, figurePoints [2]);

                break;
            case EFigureType.i3:
                transform.GetChild (0).transform.Translate (-Camera.main.transform.right * CubePointPosition.size);
                transform.GetChild (2).transform.Translate (Camera.main.transform.right * CubePointPosition.size);

                figurePoints [0].SetOneNeighbourAND (EDirection.right, figurePoints [1]);
                figurePoints [1].SetOneNeighbourAND (EDirection.left, figurePoints [0]);
                figurePoints [1].SetOneNeighbourAND (EDirection.right, figurePoints [2]);
                figurePoints [2].SetOneNeighbourAND (EDirection.left, figurePoints [1]);

                break;
            case EFigureType.sl:
                transform.GetChild (0).transform.Translate (-Camera.main.transform.up * CubePointPosition.size);
                transform.GetChild (2).transform.Translate (Camera.main.transform.right * CubePointPosition.size);

                figurePoints [0].SetOneNeighbourAND (EDirection.up, figurePoints [1]);
                figurePoints [1].SetOneNeighbourAND (EDirection.down, figurePoints [0]);
                figurePoints [1].SetOneNeighbourAND (EDirection.right, figurePoints [2]);
                figurePoints [2].SetOneNeighbourAND (EDirection.left, figurePoints [1]);

                break;
            case EFigureType.i2:
                transform.GetChild (1).transform.Translate (Camera.main.transform.right * CubePointPosition.size);
                Debug.Log ("Camera right vector " + Camera.main.transform.right.ToString());

                figurePoints [0].SetOneNeighbourAND (EDirection.right, figurePoints [1]);
                figurePoints [1].SetOneNeighbourAND (EDirection.left, figurePoints [0]);

                // Debug.Log ( transform.GetChild (0).GetComponent<CubeAttributes>().color  + " " + transform.GetChild (1).GetComponent<CubeAttributes>().color) ;

                break;
        }

        // Debug.Log (figType.ToString ());
    }

    public void SwitchColor ()
    {
        CubeColors c1, c2, newColor = colorScheme.firstColor;

        c1 = transform.GetChild (0).GetComponent<CubeAttributes>().color;
        c2 = transform.GetChild (1).GetComponent<CubeAttributes>().color;

        if ( (colorScheme.firstColor != c1) && (colorScheme.firstColor != c2) )
            newColor = colorScheme.firstColor;
        if ( (colorScheme.secondColor != c1) && (colorScheme.secondColor != c2) )
            newColor = colorScheme.secondColor;
        if ( (colorScheme.thirdColor != c1) && (colorScheme.thirdColor != c2) )
            newColor = colorScheme.thirdColor;

        //Debug.Log ("Color c1 " + c1.ToString() + " Color c2 " + c2.ToString() + " Color new " + newColor.ToString() );

        SetColors (c2, newColor);
    }

    public void SetColors ( CubeColors c1, CubeColors c2 )
    {
        switch (figureViewType) 
        {
            case EFigureViewType.edge:
            
                transform.GetChild (0).GetComponent<CubeEdges>().setColor(c1);
                transform.GetChild (1).GetComponent<CubeEdges>().setColor(c2);
                break;
                
            case EFigureViewType.transparent:
            
                transform.GetChild (0).GetComponent<CubeAttributes>().color = c1;
                transform.GetChild (0).GetComponent<Renderer>().material.SetColor("_Color", colors[c1]);
                transform.GetChild (1).GetComponent<CubeAttributes>().color = c2;
                transform.GetChild (1).GetComponent<Renderer>().material.SetColor("_Color", colors[c2]);
                break;
        }

    }

    void HideFigure ()
    {
        transform.GetChild (0).gameObject.SetActive (false);
        transform.GetChild (1).gameObject.SetActive (false);
        transform.GetChild (2).gameObject.SetActive (false);
        transform.GetChild (3).gameObject.SetActive (false);
    }

    void HideSelection ()
    {
        foreach (Transform cubeSelection in cubeSelections)
            cubeSelection.gameObject.SetActive (false);
    }

    protected void MoveFigure ( float scale = 1.0f )
    {
        float ds;
        
        ds = scale * CubePointPosition.size * Time.deltaTime * moveSpeed;
        
        switch (movement)
        {
        case EMoveFigure.up:
            translateAlongVector (Camera.main.transform.up, ds, scale);
            break;
        case EMoveFigure.right:
            translateAlongVector (Camera.main.transform.right, ds, scale);
            break;
        case EMoveFigure.down:
            translateAlongVector (-Camera.main.transform.up, ds, scale);
            break;
        case EMoveFigure.left:
            translateAlongVector (-Camera.main.transform.right, ds, scale);
            break;
        }
    }

    void translateAlongVector(Vector3 unitVector, float ds, float scale)
    {
        if (move + ds > scale * CubePointPosition.size)
        {
            transform.Translate (unitVector * (scale * CubePointPosition.size - move), Space.World);
            movement = EMoveFigure.no;
            move = 0.0f;
            redrawSelection = true;
            Debug.Log ("Movement finished");
        }
        else
        {
            transform.Translate (unitVector * ds, Space.World);
            move += ds;
        }
    }
    
    bool RayCastToBase ()
    {
        Ray ray;
        Transform previosHitCube = hit.transform; // Return null if RaycastHit is null

        for (int i = 0; i < 4; i++)
        {
            dist2base [i] = new KeyValuePair<int, float> (i, 100);
        }
        ray = new Ray (transform.GetChild (0).transform.position, Camera.main.transform.forward);
        if (Physics.Raycast (ray, out hits[0], 100))
            if (hits[0].transform != null)
                dist2base [0] = new KeyValuePair<int, float> (0, hits[0].distance);
        ray = new Ray (transform.GetChild (1).transform.position, Camera.main.transform.forward);
        if (Physics.Raycast (ray, out hits[1], 100))
            if (hits[1].transform != null)
                dist2base [1] = new KeyValuePair<int, float> (1, hits[1].distance);
        ray = new Ray (transform.GetChild (2).transform.position, Camera.main.transform.forward);
        if (Physics.Raycast (ray, out hits[2], 100))
            if (hits[2].transform != null)
                dist2base [2] = new KeyValuePair<int, float> (2, hits[2].distance);
        ray = new Ray (transform.GetChild (3).transform.position, Camera.main.transform.forward);
        if (Physics.Raycast (ray, out hits[3], 100))
            if (hits[3].transform != null)
                dist2base [3] = new KeyValuePair<int, float> (3, hits[3].distance);

            dist2base.Sort (
            delegate (KeyValuePair<int, float> firstPair, KeyValuePair<int, float> nextPair)
            {
                return firstPair.Value.CompareTo(nextPair.Value);
            }
        );
        // Debug.Log ("dist2base " + dist2base[0].ToString () + " " + dist2base[1].ToString () + " " + dist2base[2].ToString () + " " + dist2base[3].ToString ());

        // Set new hit cube
        hit = hits [dist2base [0].Key];
        childHitIndex = dist2base [0].Key;

        // If we don't hit any cube reset childHitIndex
        if (hit.transform == null)
        {
            childHitIndex = -1;
        }

        return (dist2base[0].Value > 1);
    }

    void DrawSelection ()
    {
        int i = 0;
                    
        switch (highlightType)
        {
            case EHighlightType.square:
                foreach (Transform selTransform in cubeSelections)
                {
                    selTransform.gameObject.SetActive (false);
                }

                if (hit.transform != null)
                    foreach (KeyValuePair<int, float> pair in dist2base)
                        // we check if the distance to a ground cube equals to the minimum. Because they both float we can't compare dircetly
                        if ( Mathf.Abs (pair.Value - dist2base[0].Value) < 0.0001f )
                        {
                            cubeSelections[i].position = hits[pair.Key].transform.position - (0.01f + CubePointPosition.size / 2.0f) * Camera.main.transform.forward;
                            cubeSelections[i].rotation = Camera.main.transform.rotation;
                            cubeSelections[i].gameObject.SetActive (true);
                            i++;
                        }

                redrawSelection = false;
                break;
            case EHighlightType.cube:
                Transform previosHitCube = hit.transform;
                Color c = Color.black;

                // Restore color of previous hit cube
                if ((previosHitCube != null) && (hit.transform != previosHitCube))
                    previosHitCube.GetComponent<Renderer>().material.color = hitColor; 

                // Change color of hit cube
                if ((hit.transform != null) && (hit.transform != previosHitCube) && (hit.transform.GetComponent<CubeAttributes> ().level > 0))
                {
                    hitColor = hit.transform.GetComponent<Renderer>().material.color;
                    
                    switch (hit.transform.GetComponent<CubeAttributes> ().color)
                    {
                        case CubeColors.red:
                            c = new Color (1.0f, 0f, 0f);
                            break;
                        case CubeColors.white:
                            c = new Color (194.0f/255.0f, 255.0f, 255.0f);
                            break;
                        case CubeColors.yellow:
                            c = new Color (255.0f, 255.0f, 0f);
                            break;
                    }
                    hit.transform.GetComponent<Renderer>().material.color = c;
                }
                break;
            case EHighlightType.axis:
                DrawAxis();
                break;
        }
    }

    void SetLevelTillFall ()
    {
        if (hit.transform != null)
            levelTillFall = (int)( dist2base[0].Value / CubePointPosition.size );
    }

    bool AttachFigure (int childIndex, Transform hitCube, EDirection attachDir, EDirection searchDir)
    {
        /*
        Recursively call itself to attach figure cubes concurrently.

        return True if there was a neighbour gold cube
        
        childIndex - index of a cube in figure (see CubePointPosition)
        hitCube - the nearest cube of the base figure (GameObject)
        attachDir - in this direction the figure will be attached to the baseFigure
        searchDir - which direction we came from. We do not go it again in a recursion.
        */
        EDirection dir;
        Vector3 newPosition;
        Vector3 previousPosition;
        Transform createdCube;
        bool res = false;
        bool nextres;

        createdCube = gameLogic.baseFigure.AttachFigureCube (attachDir, 
            transform.GetChild (childIndex).GetComponent<CubeAttributes> ().color, 
            hitCube);

        res = gameLogic.baseFigure.IsGoldNeighbour ( createdCube.GetComponent<CubeAttributes> () );
            
        previousPosition = hitCube.position;

        newPosition = previousPosition + CubePointPosition.size * StaticLogic.GetUnitVectorFromDirection (attachDir);

        if ((figurePoints [childIndex].xRight_ != null) && (searchDir != EDirection.left))
        {
            dir = StaticLogic.TurnDirection (transform.GetChild (figurePoints [childIndex].xRight_.i).position - transform.GetChild (childIndex).position);
            nextres = AttachFigure (figurePoints [childIndex].xRight_.i, 
                          createdCube,
                          dir,
                          EDirection.right
                         );

            if (!res)
                res = nextres;
        }

        if ((figurePoints [childIndex].xLeft_ != null) && (searchDir != EDirection.right))
        {
            dir = StaticLogic.TurnDirection (transform.GetChild (figurePoints [childIndex].xLeft_.i).position - transform.GetChild (childIndex).position);
            nextres = AttachFigure (figurePoints [childIndex].xLeft_.i, 
                          createdCube,
                          dir,
                          EDirection.left
                          );
 
            if (!res)
                res = nextres;
       }

        if ((figurePoints [childIndex].yRight_ != null) && (searchDir != EDirection.down))
        {
            dir = StaticLogic.TurnDirection (transform.GetChild (figurePoints [childIndex].yRight_.i).position - transform.GetChild (childIndex).position);
            nextres = AttachFigure (figurePoints [childIndex].yRight_.i, 
                          createdCube,
                          dir,
                          EDirection.up
                          );

            if (!res)
                res = nextres;
        }

        if ((figurePoints [childIndex].yLeft_ != null) && (searchDir != EDirection.up))
        {
            dir = StaticLogic.TurnDirection (transform.GetChild (figurePoints [childIndex].yLeft_.i).position - transform.GetChild (childIndex).position);
            nextres = AttachFigure (figurePoints [childIndex].yLeft_.i, 
                          createdCube,
                          dir,
                          EDirection.down
                          );

            if (!res)
                res = nextres;
        }

        if ((figurePoints [childIndex].zRight_ != null) && (searchDir != EDirection.backward))
        {
            dir = StaticLogic.TurnDirection (transform.GetChild (figurePoints [childIndex].zRight_.i).position - transform.GetChild (childIndex).position);
            nextres = AttachFigure (figurePoints [childIndex].zRight_.i, 
                          createdCube,
                          dir,
                          EDirection.forward
                          );

            if (!res)
                res = nextres;
        }

        if ((figurePoints [childIndex].zLeft_ != null) && (searchDir != EDirection.forward))
        {
            dir = StaticLogic.TurnDirection (transform.GetChild (figurePoints [childIndex].zLeft_.i).position - transform.GetChild (childIndex).position);
            nextres = AttachFigure (figurePoints [childIndex].zLeft_.i, 
                          createdCube,
                          dir,
                          EDirection.backward
                          );

            if (!res)
                res = nextres;
        }

        return res;
    }

    public void DrawAxis()
    {
        var points = new Vector3[2];

        if (hit.transform != null)
        {
            axis.gameObject.SetActive (true);
            
            points[0] = transform.GetChild (childHitIndex).transform.position + (CubePointPosition.size / 2.0f) * Camera.main.transform.forward;
            points[1] = hit.transform.position - (CubePointPosition.size / 2.0f) * Camera.main.transform.forward;
            
            axis.GetComponent<LineRenderer>().SetPositions(points);
        }
        else
        {
            axis.gameObject.SetActive (false);
        }
    }
}
