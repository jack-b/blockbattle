﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundEffects : MonoBehaviour
{
    public AudioSource attachingRegularCube;
    public AudioSource attachingGoldCube;
    public AudioSource desctructingRegularCube;    
    public AudioSource desctructingGoldCube;

    public void PlayingAttachingRegularCube ()
    {
        attachingRegularCube.Play ();
    }

    public void PlayingAttachingGoldCube ()
    {
        attachingGoldCube.Play ();
    }

    public void PlayingDesctructingRegularCube ()
    {
        desctructingRegularCube.Play ();
    }

    public void PlayingDesctructingGoldCube ()
    {
        desctructingGoldCube.Play ();
    }
}
