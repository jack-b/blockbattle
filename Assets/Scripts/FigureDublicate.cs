﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FigureDublicate : MonoBehaviour
{
    public GameLogic gameLogic;

    public Transform transparentCube; // transparent prefab cube for figure

    public ERotating rotating;
    public float rotationSpeed = 5.0f;
    protected float angle;
    Quaternion originalRotationValue;

    public Dictionary<CubeColors,Color> transparentColors;

    // Start is called before the first frame update
    void Start()
    {
        Transform temp;

        // TODO: Move all colors to Color Scheme
        transparentColors = new Dictionary<CubeColors,Color> ();
        transparentColors.Add ( CubeColors.red, new Color (188.0f/255.0f, 84.0f/255.0f, 84.0f/255.0f, 55.0f/255.0f) );
        transparentColors.Add ( CubeColors.white, new Color (174.0f/255.0f, 174.0f/255.0f, 174.0f/255.0f, 55.0f/255.0f) );
        transparentColors.Add ( CubeColors.yellow, new Color (255.0f/255.0f, 255.0f/255.0f, 73.0f/255.0f, 55.0f/255.0f) );
        transparentColors.Add ( CubeColors.blue, new Color (23.0f/255.0f, 140.0f/255.0f, 255.0f/255.0f, 55.0f/255.0f) );

        for (int i = 0; i < 4; i++)
        {
            temp = Instantiate(transparentCube, Vector3.zero, Quaternion.identity ) as Transform;
            temp.gameObject.layer = 11;
            temp.parent = this.gameObject.transform;
        }

        angle = 0;
        originalRotationValue = transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        if (rotating != ERotating.no)
            StaticLogic.RotateObject (ref rotating, ref angle, rotationSpeed, transform.position, transform);
    }

    public void CreateDublicate (Transform original)
    {
        int i = 0;

        transform.rotation = originalRotationValue;

        for (i = 0; i < 4; i++)
            transform.GetChild (i).gameObject.SetActive (false);


        for (i = 0; i < original.childCount; i++)
            if (original.GetChild (i).gameObject.activeSelf)
            {
                transform.GetChild (i).gameObject.SetActive (true);
                transform.GetChild (i).position = transform.position;
                transform.GetChild (i).Translate( original.GetChild (i).localPosition );
                // dirty code for easy game regime move figure to cube size to the left
                transform.GetChild (i).Translate ( -CubePointPosition.size / 2.0f * transform.right );

                transform.GetChild (i).GetComponent<Renderer>().material.SetColor("_Color", transparentColors[original.GetChild (i).GetComponent<CubeAttributes>().color]);
            }
        // Debug.Log ( original.GetChild (0).GetComponent<CubeAttributes>().color  + " " + original.GetChild (1).GetComponent<CubeAttributes>().color) ;
    }

    public void RotateDublicate (ERotating rot, float rotSpeed)
    {
        angle = 0;
        rotating = rot;
        if ( Mathf.Abs((int)rot) != 2 )
            rotating = (ERotating)(-(int)rot); // trick to mirror rotation
        rotationSpeed = rotSpeed;
    }
}
