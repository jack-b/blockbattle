﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class LevelSettingsStatic
{
    public static bool firstGold = true; // Player wins if he destroys the first gold cube

    public static EGameDifficulty gameDifficulty = EGameDifficulty.easy;

    public static ELevelNames levelName = ELevelNames.none; // load given level

    public static string levelTitle = ""; // Display this title at the begginning of the level

    // Game mechanic's settings
    public static bool nothingFall = false; // All pieces left hanging in the air

    public static int calibrate = 0; // Calibrate base figure's cube colors for small figures

    public static bool figureColorsAtStart = false; // if True figure will use given colors

    public static CubeColors[] colorsAtStart;

    public static void SetLevel (ELevelNames _levelName)
    {
        levelName = _levelName;

        Debug.Log ("Level Name: " + levelName.ToString());
        switch (levelName)
        {
            case ELevelNames.first2gold:
                levelTitle = "Dig up and break 2 shiny red gold blocks";
                firstGold = false;
                nothingFall = false;
                calibrate = 2;
                break;
            case ELevelNames.sphere:
                levelTitle = "Dig up and break the 1 shiny gold block";
                firstGold = false;
                nothingFall = false;
                break;
            case ELevelNames.candle:
                levelTitle = "Break all shiny gold blocks";
                firstGold = false;
                nothingFall = false;
                break;
            case ELevelNames.random_box:
                levelTitle = "Break all shiny gold blocks";
                firstGold = false;
                nothingFall = true;
                break;
            case ELevelNames.piramid:
                levelTitle = "Dig up and break the 1 shiny gold block";
                firstGold = false;
                nothingFall = false;
                calibrate = 2;
                break;
            case ELevelNames.gully:
                levelTitle = "Break all shiny gold blocks";
                firstGold = false;
                nothingFall = false;
                calibrate = 2;
                break;
            case ELevelNames.basic_rule:
                levelTitle = "Dig up and break the shiny gold block";
                firstGold = false;
                nothingFall = false;
                calibrate = 2;
                figureColorsAtStart = true;
                // TODO: add static color scheme to this class and make that ColorScheme Class rewrite static
                colorsAtStart = new CubeColors[] {CubeColors.red, CubeColors.yellow};
                break;
        }

        //StaticLogic.Reset ();
    }

    public static void SetLevel ()
    {
        SetLevel (levelName);
    }
}

public class LevelSettings : MonoBehaviour
{
    public bool rewriteSettings = false; // If true it rewrites static level settings on Awake

    public bool firstGold = true; // Player wins if he destroys the first gold cube

    public EGameDifficulty gameDifficulty = EGameDifficulty.easy;

    public ELevelNames levelName = ELevelNames.none; // load given level

    // Game mechanic's settings
    public bool nothingFall = false; // All pieces left hanging in the air

    public int calibrate = 0; // Calibrate base figure's cube colors for small figures
}
