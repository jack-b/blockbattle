﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeColorsArrays {
    public static string[] redColors = {"#ffc2b3", "#ffad99", "#ff9980", "#ff8566", "#ff704d", "#ff5c33", "#ff471a", "#ff3300", "#e62e00", "#cc2900", "#b32400", "#991f00"}; 
    public static string[] yellowColors = {"#fff0b3", "#ffeb99", "#ffe680", "#ffe066", "#ffdb4d", "#ffd633", "#ffd11a", "#ffcc00", "#e6b800"};
    public static string[] whiteColors = {"#ffffff", "#f2f2f2", "#e6e6e6", "#d9d9d9", "#cccccc", "#bfbfbf", "#b3b3b3"};
    /*
    public static string[] redColors = {"#ffc2b3", "#ff9980", "#ff704d", "#e62e00"}; 
    public static string[] yellowColors = {"#fff0b3", "#ffeb99", "#ffdb4d", "#ffcc00"};
    public static string[] whiteColors = {"#ffffff", "#f2f2f2", "#d9d9d9", "#cccccc"};
    */
    public static string[] purpleColors = {"#f0b3ff", "#eb99ff", "#e580ff", "#e066ff", "#db4dff", "#d633ff", "#d11aff", "#cc00ff", "#b800e6", "#a300cc", "#8f00b3"};
    public static string[] blueColors = {"#b3d9ff", "#99ccff", "#80bfff", "#66b3ff", "#4da6ff", "#3399ff", "#1a8cff", "#0080ff", "#0073e6", "#0066cc", "#8f00b3"};
} 

public class CubeColorsArraysCycle : CubeColorsArrays {
    /*
    public static new string[] redColors = {"#ffad99", "#ff704d", "#cc2900"}; 
    public static new string[] yellowColors = {"#ffeb99", "#ffdb4d", "#e6b800"};
    public static new string[] whiteColors = {"#ffffff", "#d9d9d9", "#bfbfbf"};
    */

    public static new string[] redColors = {"#ffad99", "#e62e00"}; 
    public static new string[] yellowColors = {"#ffeb99", "#ffcc00"};
    public static new string[] whiteColors = {"#f2f2f2", "#cccccc"};
} 

public class ColorScheme : MonoBehaviour
{
    public CubeColors firstColor = CubeColors.red;
    public CubeColors secondColor = CubeColors.white;
    public CubeColors thirdColor = CubeColors.yellow;

    public List<CubeColors> colors;

    void Awake()
    {
        colors = new List<CubeColors>();
        colors.Add (firstColor);
        colors.Add (secondColor);
        colors.Add (thirdColor);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
