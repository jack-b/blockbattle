﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerDestroyedCubes : MonoBehaviour
{
    public CubeBaseFigure baseFigure;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (baseFigure != null)
            transform.GetComponent<TextMeshProUGUI> ().SetText ( baseFigure.destroyedCubesPlayer.ToString () );
    }
}
