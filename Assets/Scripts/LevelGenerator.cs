﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate int CubeConstructor(int i, int j, int k);

public enum ELevelNames {none, tutorial1, first2gold, sphere, candle, random_box, piramid, gully, basic_rule};

public class LevelGenerator : MonoBehaviour
{
    public CubeConstructor regularConstructor; // link to generator functions
    public CubeConstructor goldConstructor; // link to generator function

    public Vector3Int pit = 7 * Vector3Int.one;

    public void Init (ELevelNames levelName)
    {
        switch (levelName)
        {
            case ELevelNames.tutorial1:
                regularConstructor = tutorialRegular1;
                goldConstructor = tutorialGold1;
                pit.x = 4;
                pit.y = 3;
                pit.z = 2;
                break;
            case ELevelNames.first2gold:
                regularConstructor = tutorialfirst2goldRegular;
                goldConstructor = tutorialfirst2goldGold;
                pit.x = 3;
                pit.y = 3;
                pit.z = 5;
                break;
            case ELevelNames.sphere:
                regularConstructor = levelSphereRegular;
                goldConstructor = levelSphereGold;
                pit.x = 7;
                pit.y = 7;
                pit.z = 7;
                break;
            case ELevelNames.candle:
                regularConstructor = levelСandleRegular;
                goldConstructor = levelСandleGold;
                pit.x = 3;
                pit.y = 3;
                pit.z = 8;
                break;
            case ELevelNames.random_box:
                regularConstructor = levelRandomBoxRegular;
                goldConstructor = levelRandomBoxGold;
                pit.x = 4;
                pit.y = 4;
                pit.z = 8;
                break;
            case ELevelNames.piramid:
                regularConstructor = levelPiramidRegular;
                goldConstructor = levelPiramidGold;
                pit.x = 5;
                pit.y = 5;
                pit.z = 5;
                break;
            case ELevelNames.gully:
                regularConstructor = levelGullyRegular;
                goldConstructor = levelGullyGold;
                pit.x = 5;
                pit.y = 5;
                pit.z = 5;
                break;
            case ELevelNames.basic_rule:
                regularConstructor = levelBasicRuleRegular;
                goldConstructor = levelBasicRuleGold;
                pit.x = 3;
                pit.y = 3;
                pit.z = 5;
                break;
        }
    }

    public int levelBasicRuleRegular ( int i, int j, int k )
    {
        int val = 0;
        int[,] grid = new int[,] 
        { 
            { 1, 0, 1 },
            { 2, 0, 2 },
            { 2, 2, 2 }
        };

        if (k == 0)
        {
            val = grid [i, j];
        }
        else
            val = UnityEngine.Random.Range (1, 4);

        return val;
    }

    public int levelBasicRuleGold ( int i, int j, int k )
    {
        int val = 0;

        if ( (k == 1) && (i == 2) && (j == 1) )
            val = 2;
            
        return val;
    }

    public int tutorialRegular1 ( int i, int j, int k )
    {
        int[,,] grid = new int[,,] 
        { 
            
            { 
                { 0, 0 }, { 1, 0 }, { 0, 0 } 
            },
            { 
                { 1, 0 }, { 0, 1 }, { 1, 0 } 
            },
            { 
                { 3, 0 }, { 0, 3 }, { 3, 0 } 
            },
            { 
                { 0, 0 }, { 3, 0 }, { 0, 0 } 
            }
            /*
            { 
                { 0, 0 }, { 1, 0 }, { 0, 0 } 
            },
            { 
                { 0, 0 }, { 1, 0 }, { 0, 0 } 
            },
            { 
                { 0, 0 }, { 1, 0 }, { 0, 0 } 
            }
            */
        };
        int val = 0;

        if ( ( i < 3 ) && ( j < 3 ) && ( k < 2 ) )
            val = grid [i, j, k];

        return val;
    }

    public int tutorialGold1 ( int i, int j, int k )
    {
        return 0;
    }

    public int tutorialfirst2goldRegular ( int i, int j, int k )
    {
        int[,,] grid = new int[,,] 
        { 
            
            { 
                {   0, 
                    UnityEngine.Random.Range (1, 4), 
                    UnityEngine.Random.Range (1, 4), 
                    UnityEngine.Random.Range (1, 4) }, 
                {   1, 
                    2, 
                    UnityEngine.Random.Range (1, 4), 
                    UnityEngine.Random.Range (1, 4) }, 
                {   0, 
                    3, 
                    UnityEngine.Random.Range (1, 4), 
                    UnityEngine.Random.Range (1, 4) } 
            },
            { 
                {   0, 
                    2, 
                    UnityEngine.Random.Range (1, 4), 
                    UnityEngine.Random.Range (1, 4) }, 
                {   0, 
                    1, 
                    UnityEngine.Random.Range (1, 4), 
                    UnityEngine.Random.Range (1, 4) }, 
                {   0, 
                    2, 
                    UnityEngine.Random.Range (1, 4), 
                    UnityEngine.Random.Range (1, 4) } 
            },
            { 
                {   0, 
                    3, 
                    UnityEngine.Random.Range (1, 4), 
                    UnityEngine.Random.Range (1, 4) }, 
                {   1, 
                    1, 
                    UnityEngine.Random.Range (1, 4), 
                    UnityEngine.Random.Range (1, 4) }, 
                {   0, 
                    2, 
                    UnityEngine.Random.Range (1, 4), 
                    UnityEngine.Random.Range (1, 4) } 
            }
        };
        int val = 0;

        if ( ( i < 3 ) && ( j < 3 ) && ( k < 4 ) )
            val = grid [i, j, k];

        if ( k == 4 )
            val = UnityEngine.Random.Range (1, 4);

        return val;
    }

    public int tutorialfirst2goldGold ( int i, int j, int k )
    {
        int val = 0;

        //if ( ( i == 1 ) && ( j == 1 ) && ( k == 2 ) )
        //    val = 2; //UnityEngine.Random.Range (1, 4);
        if ( ( i == 0 ) && ( j == 1 ) && ( k == 1 ) )
            val = 1; //UnityEngine.Random.Range (1, 4);
        if ( ( i == 2 ) && ( j == 1 ) && ( k == 1 ) )
            val = 1; //UnityEngine.Random.Range (1, 4);

        return val;
    }

    public int levelSphereRegular ( int i, int j, int k )
    {
        int val = 0;
        float rad = 7.6f / 2.0f;
        float distance = rad * rad - 
            ((float)i + 0.5f - (float)pit.x/2) * ((float)i + 0.5f - (float)pit.x/2) - 
            ((float)j + 0.5f - (float)pit.y/2) * ((float)j + 0.5f - (float)pit.y/2) - 
            ((float)k + 0.5f - (float)pit.z/2) * ((float)k + 0.5f - (float)pit.z/2);

        if ( distance >= 0 )
            val = UnityEngine.Random.Range (1, 4);
            
        return val;
    }

    public int levelSphereGold ( int i, int j, int k )
    {
        int val = 0;
        float rad = 1.0f / 2.0f;
        float distance = rad * rad - 
            ((float)i + 0.5f - (float)pit.x/2) * ((float)i + 0.5f - (float)pit.x/2) - 
            ((float)j + 0.5f - (float)pit.y/2) * ((float)j + 0.5f - (float)pit.y/2) - 
            ((float)k + 0.5f - (float)pit.z/2) * ((float)k + 0.5f - (float)pit.z/2);

        if ( distance >= 0 )
            val = UnityEngine.Random.Range (1, 4);

        return val;
    }

    public int levelСandleRegular ( int i, int j, int k )
    {
        int val = 0;

        if (k == 0)
        {
            if ( ( (i == 0) && (j == 2) ) || ( (i == 2) && (j == 0) ) )
                val = UnityEngine.Random.Range (1, 4);
        }
        else
        if (k != 4)
            val = UnityEngine.Random.Range (1, 4);

        return val;
    }

    public int levelСandleGold ( int i, int j, int k )
    {
        int val = 0;

        if (k == 4)
            val = UnityEngine.Random.Range (1, 4);
            
        return val;
    }

    public int levelRandomBoxRegular ( int i, int j, int k )
    {
        int val = 0;

        if ( k >= 6 )
        {
            val = UnityEngine.Random.Range (1, 4);
        }
        else
        {
            val = UnityEngine.Random.Range (0, 10);

            val = val > 2 ? 0 : val + 1;
        }

        return val;
    }

    public int levelRandomBoxGold ( int i, int j, int k )
    {
        int val = 0;

        if ( (k > 0) && (k < 5) )
        {
            val = UnityEngine.Random.Range (0, 30);
            val = val > 2 ? 0 : val + 1;
        }
            
        return val;
    }

    public int levelPiramidRegular ( int i, int j, int k )
    {
        int val = 0;

        if ( k == 4 )
        {
            val = UnityEngine.Random.Range (1, 4);
        }
        else
        {
            if ( Mathf.Abs (i - 2) + Mathf.Abs (j - 2) <= k)
            {
                val = UnityEngine.Random.Range (1, 4);
            }
        }

        return val;
    }

    public int levelPiramidGold ( int i, int j, int k )
    {
        int val = 0;

        if ( (k == 3) && (i == 2) && (j == 2) )
        {
            val = UnityEngine.Random.Range (1, 4);
        }
            
        return val;
    }

    public int levelGullyRegular ( int i, int j, int k )
    {
        int val = 0;

        if ( k == 4 )
        {
            val = UnityEngine.Random.Range (1, 4);
        }
        else
        {
            //float r = (float) (3 - k);
            //if ( (i - 2) * (i - 2) + (j - 2) * (j - 2) < r * r + 0.01f)
            if ( Mathf.Abs (i - 2) + Mathf.Abs (j - 2) > (3 - k) )
            {
                val = UnityEngine.Random.Range (1, 4);
            }
        }

        return val;
    }

    public int levelGullyGold ( int i, int j, int k )
    {
        int val = 0;

        if ( (k < 4) && ( (Mathf.Abs(i - 2) + Mathf.Abs(j - 2) + k) == 5) )
        {
            val = UnityEngine.Random.Range (1, 4);
        }
            
        return val;
    }
}
