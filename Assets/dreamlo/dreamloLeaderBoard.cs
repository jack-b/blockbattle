using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using UnityEngine.Networking;

public class dreamloLeaderBoard : MonoBehaviour {

	string dreamloWebserviceURL = "http://dreamlo.com/lb/";

	public bool IUpgradedAndGotSSL = false;
	public string privateCode = "";
	public string publicCode = "";
	
	string highScores = "";
	
	////////////////////////////////////////////////////////////////////////////////////////////////
	
	// A player named Carmine got a score of 100. If the same name is added twice, we use the higher score.
 	// http://dreamlo.com/lb/(your super secret very long code)/add/Carmine/100

	// A player named Carmine got a score of 1000 in 90 seconds.
 	// http://dreamlo.com/lb/(your super secret very long code)/add/Carmine/1000/90
	
	// A player named Carmine got a score of 1000 in 90 seconds and is Awesome.
 	// http://dreamlo.com/lb/(your super secret very long code)/add/Carmine/1000/90/Awesome
	
	////////////////////////////////////////////////////////////////////////////////////////////////
	
	
	public struct Score {
		public string playerName;
		public int score;
		public int seconds;
		public string shortText;
		public string dateString;
		public int rank;
	}
	
	void Start()
	{
		if (IUpgradedAndGotSSL) {
			dreamloWebserviceURL = "https://www.dreamlo.com/lb/";
		}
		else {
#if UNITY_WEBGL || UNITY_IOS || UNITY_ANDROID
		Debug.LogWarning("dreamlo may require https for WEBGL / IOS / ANDROID builds.");
#endif
		}

		this.highScores = "";
	}
	
	public static dreamloLeaderBoard GetSceneDreamloLeaderboard()
	{
		var go = GameObject.Find("dreamloPrefab");
		
		if (go == null) 
		{
			Debug.LogError("Could not find dreamloPrefab in the scene.");
			return null;
		}

		return go.GetComponent<dreamloLeaderBoard>();
	}
	
	public void AddScore(string playerName, int totalScore)
	{
		AddScoreWithPipe(playerName, totalScore);
	}
	
	public void AddScore(string playerName, int totalScore, int totalSeconds)
	{
		AddScoreWithPipe(playerName, totalScore, totalSeconds);
	}
	
	public void AddScore(string playerName, int totalScore, int totalSeconds, string shortText)
	{
		AddScoreWithPipe(playerName, totalScore, totalSeconds, shortText);
	}
	
	// This function saves a trip to the server. Adds the score and retrieves results in one trip.
	void AddScoreWithPipe(string playerName, int totalScore)
	{
		//Debug.Log ("HighScores-add length before: " + highScores.Length);
		playerName = Clean(playerName);
		StartCoroutine(GetRequest(dreamloWebserviceURL + privateCode + "/add-pipe/" + UnityWebRequest.EscapeURL(playerName) + "/" + totalScore.ToString()));
		//Debug.Log ("HighScores-add length after: " + highScores.Length);
	}
	
	void AddScoreWithPipe(string playerName, int totalScore, int totalSeconds)
	{
		//Debug.Log ("HighScores-add length before: " + highScores.Length);
		playerName = Clean(playerName);
		StartCoroutine(GetRequest(dreamloWebserviceURL + privateCode + "/add-pipe/" + UnityWebRequest.EscapeURL(playerName) + "/" + totalScore.ToString()+ "/" + totalSeconds.ToString()));
		//Debug.Log ("HighScores-add length after: " + highScores.Length);
	}
	
	void AddScoreWithPipe(string playerName, int totalScore, int totalSeconds, string shortText)
	{
		//Debug.Log ("HighScores-add length before: " + highScores.Length);
		playerName = Clean(playerName);
		shortText = Clean(shortText);
		
		StartCoroutine(GetRequest(dreamloWebserviceURL + privateCode + "/add-pipe/" + UnityWebRequest.EscapeURL(playerName) + "/" + totalScore.ToString() + "/" + totalSeconds.ToString()+ "/" + shortText));
		//Debug.Log ("HighScores-add length after: " + highScores.Length);
	}
	
	void GetScores()
	{
		highScores = "";
		StartCoroutine(GetRequest(dreamloWebserviceURL +  publicCode  + "/pipe"));
	}

	void GetScoresTimeAsc(int startIndex, int amount)
	{
		highScores = "";
		
		if (startIndex < 0) startIndex = 0;

		if (amount <= 0) return;

		StartCoroutine(GetRequest(dreamloWebserviceURL +  publicCode  + "/pipe-seconds-asc/" + startIndex + "/" + amount));
	}

	void GetScoresTimeAsc()
	{
		highScores = "";
		StartCoroutine(GetRequest(dreamloWebserviceURL +  publicCode  + "/pipe-seconds-asc"));
	}

	void GetSingleScore(string playerName)
	{
		highScores = "";
		StartCoroutine(GetRequest(dreamloWebserviceURL +  publicCode  + "/pipe-get/" + UnityWebRequest.EscapeURL(playerName)));
	}

	IEnumerator GetRequest(string url)
	{
		// Something not working? Try copying/pasting the url into your web browser and see if it works.
		// Debug.Log(url);

		using (UnityWebRequest www = UnityWebRequest.Get(url))
		{
			yield return www.SendWebRequest();
			highScores = www.downloadHandler.text;
		}
	}
	
	
	public string[] ToStringArray()
	{
		if (this.highScores == null) return null;
		if (this.highScores == "") return null;
		
		var rows = this.highScores.Split(new char[] {'\n'}, System.StringSplitOptions.RemoveEmptyEntries);
		return rows;
	}
	
	public List<Score> ToListLowToHigh()
	{
		var scoreList = this.ToScoreArray();
		
		if (scoreList == null) return new List<Score>();
		
		var genericList = new List<Score>(scoreList);
			
		genericList.Sort((x, y) => x.score.CompareTo(y.score));
		
		return genericList;
	}
	
	public List<Score> ToListHighToLow()
	{
		Debug.Log ("HighScores length before: " + highScores.Length);
		var scoreList = this.ToScoreArray();
		
		if (scoreList == null) return new List<Score>();

		List<Score> genericList = new List<Score>(scoreList);
			
		genericList.Sort((x, y) => y.score.CompareTo(x.score));
		Debug.Log ("HighScores length after: " + highScores.Length);
		
		return genericList;
	}

	public Score[] ToScoreArray()
	{
		var rows = ToStringArray();
		if (rows == null) return null;
		
		int rowcount = rows.Length;
		
		if (rowcount <= 0) return null;

		return ToScoreArray(0, rowcount);
	}

	public Score[] ToScoreArray(int startIndex, int amount)
	{
		var rows = ToStringArray();
		if (rows == null) return null;
		
		int rowcount = rows.Length;
		
		if (rowcount <= 0) return null;

		if (startIndex < 0) startIndex = 0;
		
		int max = startIndex + amount;

		if (rowcount < max) max = rowcount;

		var scoreList = new Score[max - startIndex];
		
		for (int i = startIndex; i < max; i++)
		{
			var values = rows[i].Split(new char[] {'|'}, System.StringSplitOptions.None);
			
			var current = new Score();
			current.playerName = values[0];
			current.score = 0;
			current.seconds = 0;
			current.shortText = "";
			current.dateString = "";
			if (values.Length > 1) current.score = CheckInt(values[1]);
			if (values.Length > 2) current.seconds = CheckInt(values[2]);
			if (values.Length > 3) current.shortText = values[3];
			if (values.Length > 4) current.dateString = values[4];
			scoreList[i - startIndex] = current;
		}
		
		return scoreList;
	}
	
	public Score[] ToScoreArrayAroundTime(int seconds, int amount)
	{
		GetScoresTimeAsc ();

		var rows = ToStringArray();
		if (rows == null) return null;
		
		int rowcount = rows.Length;
		
		if (rowcount <= 0) return null;

		int startIndex = 0;

		bool flag = true;
		int i = 0;

		while (flag)
		{
			var values = rows[i].Split(new char[] {'|'}, System.StringSplitOptions.None);
			
			if (values.Length > 2) 
				if ( CheckInt(values[2]) > seconds )
					flag = false;

			i++;

			if ( i >= rowcount )
				flag = false;
		}

		startIndex = (i - 1) - ( (i - 1) % 10 );

		int max = startIndex + amount;

		if (rowcount < max) max = rowcount;

		var scoreList = new Score[max - startIndex];
		
		for (i = startIndex; i < max; i++)
		{
			var values = rows[i].Split(new char[] {'|'}, System.StringSplitOptions.None);
			
			var current = new Score();
			current.playerName = values[0];
			current.score = 0;
			current.seconds = 0;
			current.shortText = "";
			current.dateString = "";
			current.rank = i + 1;
			if (values.Length > 1) current.score = CheckInt(values[1]);
			if (values.Length > 2) current.seconds = CheckInt(values[2]);
			if (values.Length > 3) current.shortText = values[3];
			if (values.Length > 4) current.dateString = values[4];
			scoreList[i - startIndex] = current;
		}
		
		return scoreList;
	}
	
	// Keep pipe and slash out of names
	
	string Clean(string s)
	{
		s = s.Replace("/", "");
		s = s.Replace("|", "");
		return s;
		
	}
	
	int CheckInt(string s)
	{
		int x = 0;
		
		int.TryParse(s, out x);
		return x;
	}
	
}