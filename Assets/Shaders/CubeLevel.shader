﻿Shader "Custom/CubeLevel"

{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Base (RGB) Trans (A)", 2D) = "white" {} 
        _BlendTex ("Blend (RGB)", 2D) = "white"
        _BlendAlpha ("Blend Alpha", float) = 0
    }
    SubShader
    {
        Tags { "Queue"="Geometry-9" "IgnoreProjector"="True" "RenderType"="Transparent" }
        Lighting Off
        LOD 200
        Blend SrcAlpha OneMinusSrcAlpha
  
        CGPROGRAM
        #pragma surface surf Lambert

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        fixed4 _Color;
        sampler2D _MainTex;
        sampler2D _BlendTex;
        float _BlendAlpha;
  
        struct Input {
          float2 uv_MainTex;
        };
  
        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        void surf (Input IN, inout SurfaceOutput o) {
          fixed4 c = _Color;
          c = ( ( 1 - _BlendAlpha ) * tex2D( _MainTex, IN.uv_MainTex ) + _BlendAlpha * tex2D( _BlendTex, IN.uv_MainTex ) ) * _Color;
          o.Albedo = c.rgb;
          o.Alpha = c.a;
//          fixed4(lerp(tex.rgb, _Color.rgb, _Color.a), 1);
        }

        ENDCG
    }
    Fallback "Transparent/VertexLit"
}